/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   newmain.cpp
 * Author: jarda
 *
 * Created on 26. listopadu 2017, 19:58
 */

#include <cstdlib>
#include <iostream>

#include "trie-2/trie.hpp"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    trie trie;
    trie.insert("a");
        trie.insert("aa");
            trie.insert("aaa");
    trie.insert("aabb");
    trie.insert("aabab");
    trie.insert("aaaab");
    trie.insert("aqqq");
    word_cursor lala=trie.get_word_cursor();
   
    vector<std::string> hzuu=trie.search_by_prefix("aa");
    for(int i=0;i<hzuu.size();i++){
    
        std :: cout << hzuu.at(i);
    }
    return 0;
}

