#include <istream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <iostream>
#include <regex>
#include "small1.hpp"

std::pair<int, int> parse_matrix(std::istream& in){
    int rowCounter = 0,columnCounter = 0,columnCounterTemp = 0;
    char readChar,lastChar;
    while (in.peek()!= EOF){
        
        readChar=in.get();
       /* std::cout << "    |";
        std::cout << int (readChar);
        std::cout << "  ";
       */
        
        if (readChar==('\n')){
            rowCounter++;
            columnCounterTemp++;
            
            
            if (columnCounter!=columnCounterTemp && columnCounter!=0){
            throw std::invalid_argument("");
            

        
        }
            columnCounter=columnCounterTemp;
            columnCounterTemp=0;
            
        }else{
            if((readChar==(' ') || readChar == char(9) ) &&  (' '!=lastChar )&& (lastChar!= char(9))){
              columnCounterTemp++;  
              
            
            }
            
        }
      
        
        lastChar=readChar;
    }
    
    if (columnCounter!=columnCounterTemp && columnCounter!=0 &&columnCounterTemp!=0){
            throw std::invalid_argument("");
            
        
        }
    return std::make_pair(rowCounter,columnCounter);
   
}

void print_table(std::ostream& out, const std::vector<std::string>& vec) {
    int sizeOfMaxElement=0,numberOfElements=vec.size();
    std::string border="",output="";
     for (int n=0; n<numberOfElements;n++) {
         if(vec.at(n).length()>sizeOfMaxElement){
             sizeOfMaxElement=vec.at(n).length();
         
         
         }
        
        
    }
 
    for(int i=0; i<sizeOfMaxElement*2+5;i++){
        border+="-";
    
    }
    
    
    // border+="\n";
    
    
     output=border+"\n"; 
      for (int y=0; y<numberOfElements;y++) {
         output+="|";
         for (int x=0; x<(sizeOfMaxElement-vec.at(y).length())+1;x++){
             output+=" ";
             
             
         
         }        
         output+=vec.at(y);
         if((y+1)%2==0){
             output+="|\n";
         
         }
        
    }
     
   if(numberOfElements % 2!=0){
          output+="|";
         for (int m=0; m<(sizeOfMaxElement+1);m++){
             output+=" ";
             
             
         
         }        
          output+="|\n";

   }
    
    
    
   
  output+=border+"\n";

  out << output;
    
}

bool validate_line(const std::string& str) {
    std::string hexNumber,decimalNumber,octalNumber,condition;
    int position=0;
       

    for(int i=0;i<str.length();i++){
        if(str.at(i)==' '){position++;i++;}
        if(i<str.length()){
        switch(position){
            case 0 :
                condition+=str.at(i);
                break;

            case 1 :
                octalNumber+=str.at(i);
                break;

            case 2 :    
                decimalNumber+=str.at(i);
                break;
                
            case 3 :
                hexNumber+=str.at(i);
                break;
        }    
        }   
    
    }
    
    for (int x=0; x<octalNumber.length();x++){
        if(int (octalNumber.at(x)) > 57 || int(octalNumber.at(x)) < 48){
            return false;
        }
    
    }

    for (int x=0; x<decimalNumber.length();x++){
        if(int (decimalNumber.at(x)) > 57 || int(decimalNumber.at(x)) < 48){
            return false;
        }
    
    }
    
    for (int x=0; x<hexNumber.length();x++){
        if((int (hexNumber.at(x)) > 57 || int(hexNumber.at(x)) < 48)&& !(x==1 && hexNumber.at(x)==('x')&& hexNumber.at(0)==('0'))){
            if ((int (hexNumber.at(x)) > 70 || int(hexNumber.at(x)) < 65)) {
                if ((int (hexNumber.at(x)) > 102 || int(hexNumber.at(x)) < 97)) {
                     return false;
                }

            }

            
           
        }
    
    }
    
    if((decimalNumber!="" && hexNumber!="" && octalNumber!="")&&(condition=="true" || condition=="false")){
   if(condition == "true"){
       try{
       if(std::stoi(octalNumber,0,8)==std::stoi(decimalNumber,0,10)&&std::stoi(decimalNumber,0,10)==std::stoi(hexNumber,0,16)){
           return true;
       }else {return false;}
       }catch(...){
           return false;
       }
    
    }else{
      try{ 
      if(std::stoi(octalNumber,0,8)!=std::stoi(decimalNumber,0,10)||std::stoi(decimalNumber,0,10)!=std::stoi(hexNumber,0,16)||std::stoi(octalNumber,0,8)!=std::stoi(hexNumber,0,16)){
           return true;
       }else {return false;}
      } catch(...){
      
          return false;
      }
    
    
    }
        
    }
    return false;
}

std::string max_number(std::istream& in) {
    std::string buffer,currentMax="";
    
    while(in.peek()!= EOF)
     {
        if(in.peek()!=(',')){
            buffer+=in.get();
        }else{
            in.get();
            if(buffer.length()>currentMax.length()){
               currentMax=buffer;
               buffer="";
               
               
               
               
            }else{
                if (buffer.length()==currentMax.length()){
                    for(int x=0 ;x<buffer.length();x++){
                        if(int(buffer.at(x))>int(currentMax.at(x))){
                            currentMax=buffer;
                            buffer="";
                            break;
                        
                        }
                    
                    
                    }
                    
                }
                buffer="";
            }
        }
    }
     if(buffer.length()>currentMax.length()){
               currentMax=buffer;
               buffer="";
               
               
               
               
            }else{
                if (buffer.length()==currentMax.length()){
                    for(int x=0 ;x<buffer.length();x++){
                        if(int(buffer.at(x))>int(currentMax.at(x))){
                            currentMax=buffer;
                            buffer="";
                            break;
                        
                        }
                    
                    
                    }
                    
                }
                buffer="";
            }
    return currentMax;
    
}
